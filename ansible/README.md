# iac

This is our ansible project, initially intended to automatize the deployment of the minimal django app in ../poc . Of course we can also see that as a minimal ansible test work.

How can you use it:

First, you do not need to "compile" the python app in ../poc. It deploys actually a development instance.

It has the usual project configuration (requirements.txt, intended to create a local virtualenv). So we need a currently
(2023-11-21) uptodate python3, pip and virtualenv modules. And nothing more.

To try it, use the following commands:

* Create the virtual environment with the command ```python3 -m venv pyenv```. That creates the virtual environment in the local "pyenv" directory.
* Activate this virtual environment by the command ```. ./pyenv/bin/activate``` (might be a little bit different on windows, afaik there you have some .cmd or so).
* Upgrade it to the latest pip by the command ```python -m pip install --upgrade pip```.
* Finally, install the project requirementa with the command ```python -m pip install -r requirements.txt```.

You can deactivate the current virtual environment with the ```deactivate``` command any time.

If everything is ok, then now an

```
ansible-playbook -i ./inv/ playbooks/pypoc_setup.yml -v
```

command set up the ../poc app in a remove server. Note, here in the inventory ./inv/, we use actually my development configuration, so feel free to edit it before you try that.

You can also execute the ```pypoc_start``` or ```pypoc_stop playbooks``` to start or stop the systemd service.

If you have gnu make in the project path, then the

```make pypoc_start```

command does all of these at once. You can also execute the other playbooks with this abbreviation. Actually, the "magical"
looking part of the ```Makefile``` is doing that all .yml files in the ./playbooks/ directory becomes a make target, executing it as an ansible playbook.
