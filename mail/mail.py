import smtplib, ssl
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

smtp_server = "smtp.freemail.hu"
smtp_port = 587  # For STARTTLS
mailaddr = "redacted@freemail.hu"
password = "redacted"

target_addrs = ["horvath.akos.peter@gmail.com", "dennis.meissner@init.de", "frank.dornheim@init.de"]

# creating the html message with an attached pdf (my cv)
msg = MIMEMultipart()
msg['From'] = mailaddr
msg['To'] = COMMASPACE.join(target_addrs)
#msg['To'] = target_addr
msg['Date'] = formatdate(localtime=True)
msg['Subject'] = "bitte zur HR weiterleiten"

text_body = """Das ist nur eine text mail body."""
html_body = """\
<html>
  <head></head>
  <body>
    <p>Das ist das HTML Mail Body.</p>
    <h5>Das ist ein h5, default css zu sehen</h5>
  </body>
</html>
"""

path = "Peter Horvath LONG CV English.pdf"
msg.attach(MIMEText(text_body, 'text'))
msg.attach(MIMEText(html_body, 'html'))

part = MIMEBase("application", "pdf")
with open(path, 'rb') as file:
  part.set_payload(file.read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', 'attachment; filename={}'.format(Path(path).name))
msg.attach(part)

# Create a secure SSL context
context = ssl.create_default_context()
context.check_hostname = False

# Try to log in to server and send email
#server = smtplib.SMTP(smtp_server, smtp_port)
server = smtplib.SMTP(smtp_server, smtp_port)
server.connect(smtp_server, smtp_port)
server.ehlo() # Can be omitted
server.starttls(context=context) # Secure the connection
# server.ehlo() # Can be omitted
server.login(mailaddr, password)
server.sendmail("redacted@freemail.hu", "horvath.akos.peter@gmail.com", msg.as_string())
server.quit()
